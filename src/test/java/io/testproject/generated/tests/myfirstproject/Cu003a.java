package io.testproject.generated.tests.myfirstproject;

import io.testproject.sdk.drivers.ReportingDriver;
import io.testproject.sdk.drivers.web.RemoteWebDriver;
import io.testproject.sdk.interfaces.junit5.ExceptionsReporter;
import java.lang.Exception;
import java.lang.Override;
import java.lang.String;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * This class was automatically generated by TestProject
 * Project: My first Project
 * Test: CU-003a
 * Generated by: JUAN PABLO GUERRA PORRAS (jpguerra81@ucatolica.edu.co)
 * Generated on Mon Oct 11 21:35:23 GMT 2021.
 */
@DisplayName("CU-003a")
public class Cu003a implements ExceptionsReporter {
  public static WebDriver driver;

  @BeforeAll
  static void setup() throws Exception {
    driver = new RemoteWebDriver("i12KZoMIxV0R96MtlZkbldyAbQgyi4GmaMDzLd6YES8", new ChromeOptions(), "JUAN PABLO GUERRA PORRAS");
  }

  /**
   * In order to upload the test to TestProject need to un-comment @ArgumentsSource and set in comment the @MethodSource
   * @org.junit.jupiter.params.provider.ArgumentsSource(io.testproject.sdk.interfaces.parameterization.TestProjectParameterizer.class) */
  @DisplayName("CU-003a")
  @ParameterizedTest
  @MethodSource("provideParameters")
  void execute(String ApplicationURL, String usuario, String contrasena) throws Exception {
    // set timeout for driver actions (similar to step timeout)
    driver.manage().timeouts().implicitlyWait(15000, TimeUnit.MILLISECONDS);
    By by;
    boolean booleanResult;

    // 1. Navigate to '{{ApplicationURL}}'
    //    Navigates the specified URL (Auto-generated)
    GeneratedUtils.sleep(500);
    driver.navigate().to(ApplicationURL);

    // 2. Click 'Registrarse'
    GeneratedUtils.sleep(500);
    by = By.xpath("//a[. = 'Registrarse']");
    driver.findElement(by).click();

    // 3. Click 'first_name'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#id_first_name");
    driver.findElement(by).click();

    // 4. Click 'last_name'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#id_last_name");
    driver.findElement(by).click();

    // 5. Click 'username'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#id_username");
    driver.findElement(by).click();

    // 6. Click 'password'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#id_password");
    driver.findElement(by).click();

    // 7. Click 'address'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#id_address");
    driver.findElement(by).click();

    // 8. Click 'mobile'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#id_mobile");
    driver.findElement(by).click();

    // 9. Click 'Iniciar sesión'
    GeneratedUtils.sleep(500);
    by = By.xpath("//a[. = 'Iniciar sesión']");
    driver.findElement(by).click();

    // 10. Type '{{usuario}}' in 'username'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#id_username");
    driver.findElement(by).sendKeys(usuario);

    // 11. Type '{{contrasena}}' in 'password'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#id_password");
    driver.findElement(by).sendKeys(contrasena);

    // 12. Click 'INPUT'
    GeneratedUtils.sleep(500);
    by = By.xpath("//input[4]");
    driver.findElement(by).click();

    // 13. Click 'Perfil'
    GeneratedUtils.sleep(500);
    by = By.xpath("//a[. = 'Perfil']");
    driver.findElement(by).click();

  }

  @Override
  public ReportingDriver getDriver() {
    return (ReportingDriver) driver;
  }

  @AfterAll
  static void tearDown() {
    if (driver != null) {
      driver.quit();
    }
  }

  private static Stream provideParameters() throws Exception {
    return Stream.of(Arguments.of("http://192.168.0.2:8000","juan2","123"));
  }
}
