using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using System.Threading;
using System;
using TestProject.SDK;
using TestProject.SDK.Tests.Helpers;
using TestProject.SDK.Tests;
using TestProject.Common.Attributes;
using TestProject.Common.Enums;

namespace TestProject.Generated.Tests.MyFirstProject
{
	/// <summary>
	/// This class was automatically generated by TestProject
	/// Project: My first Project
	/// Test: CU-003a
	/// Generated by: JUAN PABLO GUERRA PORRAS (jpguerra81@ucatolica.edu.co)
	/// Generated on: 10/11/2021 21:35:30
	/// </summary>
	public class CU003A : IWebTest
	{
		[ParameterAttribute(Description = "Auto generated application URL parameter", DefaultValue = "http://192.168.0.2:8000", Direction = ParameterDirection.Input)]
		public string ApplicationURL;
		[ParameterAttribute(DefaultValue = "juan2", Direction = ParameterDirection.Input)]
		public string usuario;
		[ParameterAttribute(DefaultValue = "123", Direction = ParameterDirection.Input)]
		public string contrasena;
		public ExecutionResult Execute(WebTestHelper helper)
		{
			var driver = helper.Driver;
			var report = helper.Reporter;
			bool boolResult;
			By by;
			
			// set timeout for driver actions (similar to step timeout)
			driver.Timeout = 15000;
			
			 // 1. Navigate to '{{ApplicationURL}}'
			// Navigates the specified URL (Auto-generated)
			// Add step sleep time (Before)
			Thread.Sleep(500);
			boolResult = driver.TestProject().NavigateToURL(ApplicationURL);
			report.Step(string.Format("Navigate to '{0}'", ApplicationURL), boolResult, TakeScreenshotConditionType.Failure);
			
			 // 2. Click 'Registrarse'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.XPath("//a[. = 'Registrarse']");
			boolResult = driver.TestProject().Click(by);
			report.Step("Click 'Registrarse'", boolResult, TakeScreenshotConditionType.Failure);
			
			 // 3. Click 'first_name'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#id_first_name");
			boolResult = driver.TestProject().Click(by);
			report.Step("Click 'first_name'", boolResult, TakeScreenshotConditionType.Failure);
			
			 // 4. Click 'last_name'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#id_last_name");
			boolResult = driver.TestProject().Click(by);
			report.Step("Click 'last_name'", boolResult, TakeScreenshotConditionType.Failure);
			
			 // 5. Click 'username'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#id_username");
			boolResult = driver.TestProject().Click(by);
			report.Step("Click 'username'", boolResult, TakeScreenshotConditionType.Failure);
			
			 // 6. Click 'password'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#id_password");
			boolResult = driver.TestProject().Click(by);
			report.Step("Click 'password'", boolResult, TakeScreenshotConditionType.Failure);
			
			 // 7. Click 'address'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#id_address");
			boolResult = driver.TestProject().Click(by);
			report.Step("Click 'address'", boolResult, TakeScreenshotConditionType.Failure);
			
			 // 8. Click 'mobile'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#id_mobile");
			boolResult = driver.TestProject().Click(by);
			report.Step("Click 'mobile'", boolResult, TakeScreenshotConditionType.Failure);
			
			 // 9. Click 'Iniciar sesión'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.XPath("//a[. = 'Iniciar sesión']");
			boolResult = driver.TestProject().Click(by);
			report.Step("Click 'Iniciar sesión'", boolResult, TakeScreenshotConditionType.Failure);
			
			 // 10. Type '{{usuario}}' in 'username'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#id_username");
			boolResult = driver.TestProject().TypeText(by, usuario);
			report.Step(string.Format("Type '{0}' in 'username'", usuario), boolResult, TakeScreenshotConditionType.Failure);
			
			 // 11. Type '{{contrasena}}' in 'password'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#id_password");
			boolResult = driver.TestProject().TypeText(by, contrasena);
			report.Step(string.Format("Type '{0}' in 'password'", contrasena), boolResult, TakeScreenshotConditionType.Failure);
			
			 // 12. Click 'INPUT'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.XPath("//input[4]");
			boolResult = driver.TestProject().Click(by);
			report.Step("Click 'INPUT'", boolResult, TakeScreenshotConditionType.Failure);
			
			 // 13. Click 'Perfil'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.XPath("//a[. = 'Perfil']");
			boolResult = driver.TestProject().Click(by);
			report.Step("Click 'Perfil'", boolResult, TakeScreenshotConditionType.Failure);
			
			return ExecutionResult.Passed;
		}
	}
}